var mongoose = require('mongoose');

require('../all-models').toContext(global);


//------------------------
// ADD SEEDS BELOW
//------------------------


// suggested module for generating fake contextual data
// var Faker = require('faker');


// For Example

// CoolUser.create([
//   { name: 'andy', age: 24 },
//   { name: 'alex', age: 23 },
//   { name: Faker.name.firstName(), age: Faker.random.number() }
// ])

// .then(() => {
//   console.log("Seed complete!")  
//   mongoose.connection.close();
// });

// be sure to close the connection once the queries are done

Movie.create([
    {title: 'Aladdin', rating: 'G', release_date: '25-Nov-1992'},
    {title: 'The Terminator', rating: 'R', release_date: '26-Oct-1984'},
    {title: 'When Harry Met Sally', rating: 'R', release_date: '21-Jul-1989'},
    {title: 'The Help', rating: 'PG-13', release_date: '10-Aug-2011'},
    {title: 'Chocolat', rating: 'PG-13', release_date: '5-Jan-2001'},
    {title: 'Amelie', rating: 'R', release_date: '25-Apr-2001'},
    {title: '2001: A Space Odyssey', rating: 'G', release_date: '6-Apr-1968'},
    {title: 'The Incredibles', rating: 'PG', release_date: '5-Nov-2004'},
    {title: 'Raiders of the Lost Ark', rating: 'PG', release_date: '12-Jun-1981'},
    {title: 'Chicken Run', rating: 'G', release_date: '21-Jun-2000'}
])
.then(() => {
  console.log("Seed complete!")
  mongoose.connection.close();
});