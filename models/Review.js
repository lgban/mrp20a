var mongoose = require('mongoose');
var Schema = mongoose.Schema;

if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'), {
    useNewUrlParser: true, useUnifiedTopology: true
  });
}


var newSchema = new Schema({
  'body': { type: String }
});

module.exports = newSchema;
