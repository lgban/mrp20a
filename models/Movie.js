var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const Review = require('./Review');

if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'), {
    useNewUrlParser: true, useUnifiedTopology: true
  });
}


var newSchema = new Schema({
  
  'title': { type: String },
  'description': { type: String },
  'rating': { type: String },
  'release_date': { type: Date },
  'reviews': [Review],
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});



module.exports = mongoose.model('Movie', newSchema);
