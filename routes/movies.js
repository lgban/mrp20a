var express = require('express');
var router = express.Router();
require('../models/all-models').toContext(global);
var moment = require('moment');

router.get('/', function(req, res, next) {
  Movie.find({}).then(movies =>
    res.render('movies/index', { movies: movies, moment: moment })
  );
});

router.get('/:id', function(req, res, next) {
  Movie.findById(req.params.id).populate('reviews').then(movie =>
    res.render('movies/show', { movie: movie, moment: moment })
  );
});

router.post('/:id/reviews', function(req, res, next) {
  Movie.findById(req.params.id).then(movie => {
      movie.reviews.push({body: req.body.body});
      movie.save();
      res.render('movies/show', { movie: movie, moment: moment })
  });
});

module.exports = router;
